//var windowHome = Titanium.UI.currentWindow;
var windowHome = Titanium.UI.currentWindow({
	url:"HomeWindow.js"
});

var tabGroup = Titanium.UI.createTabGroup();

var tabHome = Titanium.UI.createTab({
	title:"Home",
	window:windowHome
});

var windowProfile = Titanium.UI.createWindow({
	title:"Profile",
	backgroundColor:"white",
	url:"ProfileWindow.js"
});

var tabProfile = Titanium.UI.createTab({
	title:"Profile",
	window:windowProfile
});

var windowCourse = Titanium.UI.createWindow({
	title:"Course",
	backgroundColor:"white",
	url:"CourseWindow.js"
});

var tabCourse = Titanium.UI.createTab({
	title:"Course",
	window:windowCourse
});

tabGroup.addTab(tabHome);

tabGroup.addTab(tabProfile);

tabGroup.addTab(tabCourse);

tabGroup.open();

