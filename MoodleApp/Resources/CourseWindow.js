var windowCourse = Titanium.UI.currentWindow;

var lableSLIIT3 = Ti.UI.createLabel({
	text:"Course Categories",
	color:"orange",
	font:{fontSize:16},
	height:Ti.UI.SIZE,
	width:Ti.UI.SIZE,
	top:20,
	//left : myLeft,
	//textAlign:'left'
});

windowCourse.add(lableSLIIT3);

var tableCourseData = [
	{title:"Orientation Program", className:"CourseTable", hasDetail:true},
	{title:"Faculty of Computing", className:"CourseTable", hasDetail:true},
	{title:"Faculty of Engineering", className:"CourseTable", hasDetail:true},
	{title:"Faculty of Business", className:"CourseTable", hasDetail:true},
	{title:"Faculty of Postgraduate Studies and Research", className:"CourseTable", hasDetail:true},
	{title:"Professional Development Programs", className:"CourseTable", hasDetail:true},
	{title:"Research Center", className:"CourseTable", hasDetail:true},
	{title:"Digital Library", className:"CourseTable", hasDetail:true},
	{title:"Career Guidance", className:"CourseTable", hasDetail:true},
	{title:"Student Communities", className:"CourseTable", hasDetail:true},
	{title:"Staff Resources", className:"CourseTable", hasDetail:true},
	{title:"Other", className:"CourseTable", hasDetail:true}
];

var tableCourse = Titanium.UI.createTableView({
	data:tableCourseData,
	top:80
});

windowCourse.add(tableCourse);

tableCourse.addEventListener("click", function(e){
	if (e.index == 1)
	{
		var windowFacOfCom = Titanium.UI.createWindow({
			title:e.source.title,
			backgroundColor:"white",
			url:"FacOfCom.js"
		});
		windowFacOfCom.open();
	}
});